# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.2](https://gitlab.com/cptpackrat/crypted/compare/v2.0.1...v2.0.2) (2021-05-08)


### Housekeeping

* Updated tooling. ([f97ce94](https://gitlab.com/cptpackrat/crypted/commit/f97ce944fde83008bb48d40362c5c8d5f3ce10bb))
* Updated dependencies. ([ce4055a](https://gitlab.com/cptpackrat/crypted/commit/ce4055a630e8d381cb1dd9132c4a3e1e482225bc))

## [2.0.1](https://gitlab.com/cptpackrat/crypted/compare/v2.0.0...v2.0.1) (2020-03-31)


### Housekeeping

* Switched linter to standardx for better TS support. ([2f0c010](https://gitlab.com/cptpackrat/crypted/commit/2f0c010199e73085cda32ca252d8e5300c63e645))
* Tweaked README. ([c59f1d3](https://gitlab.com/cptpackrat/crypted/commit/c59f1d39df72fb9ee7fa479ee4634a9b247275b1))
* Updated typescript target to ES2018. ([021f4bf](https://gitlab.com/cptpackrat/crypted/commit/021f4bf97f425da85425f37c398c501950cfec20))
* Updated dependencies. ([ff7d8a0](https://gitlab.com/cptpackrat/crypted/commit/ff7d8a0e8240e0561ea668d3f98b71c9b317fbb8))
* Various CI changes. ([01a7418](https://gitlab.com/cptpackrat/crypted/commit/01a7418584d39035f011b3dced35ecb89d602762))

## [2.0.0](https://gitlab.com/cptpackrat/crypted/compare/v1.0.0...v2.0.0) (2020-01-03)


### BREAKING CHANGES

* Switched to using the V8 serialiser; this change is not backwards-compatible
with the previous release, or any ciphertexts created using the previous release.
* Switched from CBC to GCM mode of operation; this change is not
backwards-compatible with the previous release, or any ciphertexts created using
the previous release.

### Improvements

* Switched from AES-256-CBC to AES-256-GCM. ([51b7b2c](https://gitlab.com/cptpackrat/crypted/commit/51b7b2c8a30203e7f0e25b644148a9291c2b4264))
* Switched to the V8 serializer, increasing the number of types that can be serialised. ([46a5946](https://gitlab.com/cptpackrat/crypted/commit/46a5946aee9e8e0d6578a3929037af33b2e50edc))


### Housekeeping

* Updated dependencies. ([f78ae8b](https://gitlab.com/cptpackrat/crypted/commit/f78ae8b4c6ffad0ae9c0dca54b6ec1ca577d3f9d))
