# @nfi/crypted
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

Simple wrapper for encryption/decryption of V8-serialisable data structures.

## Installation
```
npm install @nfi/crypted
```

## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/crypted).

## Usage Example
```js
const { encrypt, decrypt, keygenSync } = require('@nfi/crypted')

const key = keygenSync('password', 'salt')
const msg = {
  foo: 'test',
  bar: 123456
}

const enc = encrypt(key, msg)
const dec = decrypt(key, enc)

console.log(enc) // iaeSPY4cCCeLHwyRGeZ4MPsHDTrpkkmV4M8TdB9O4rxJlBaAYYccd5iIVahMRb2S
console.log(dec) // { foo: 'test', bar: 123456 }
```

## Implementation
- Internally, `encrypt`/`decrypt` use AES-256-GCM with randomised IVs, and `keygen`/`keygenSync`
use 100,000 iterations of PBKDF2-HMAC-SHA512.
- Payloads are serialised using `v8.serialize` and `v8.deserialize`, so any caveats regarding the
types of data that will successfully round-trip through those two functions also apply here.
- The ciphertexts returned by `encrypt` are a base64-encoded concatenation of the IV, encrypted
payload, and authentication tag.

## Typescript
When used with Typescript, `encrypt` returns a tagged string type called `Crypted`, which is
functionally identical to a string but allows type information about the original payload to
be preserved across subsequent calls to `decrypt`:
```ts
const msg = 12345             // const msg: number
const enc = encrypt(key, msg) // const enc: Crypted<number>
const dec = decrypt(key, enc) // const dec: number
```
This can be used to provide type safety hints for ciphertexts based on their expected contents:
```ts
function example(enc: Crypted<{
  val1: boolean
  val2: string
}>) {
  const dec = decrypt(key, enc)
  if (dec.val1) {
    console.log(dec.val2)
  }
}

example(encrypt(key, { val1: true, val2: 'example' })) // compiles ok
example(encrypt(key, 12345))                           // type error
```

[npm-image]: https://img.shields.io/npm/v/@nfi/crypted.svg
[npm-url]: https://www.npmjs.com/package/@nfi/crypted
[pipeline-image]: https://gitlab.com/cptpackrat/crypted/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/crypted/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/crypted/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/crypted/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
