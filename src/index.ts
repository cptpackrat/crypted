import { serialize, deserialize } from 'v8'
import {
  randomBytes,
  createCipheriv,
  createDecipheriv,
  pbkdf2,
  pbkdf2Sync,
  BinaryLike
} from 'crypto'

/** Tagged string type used to allow type information to be
  * preserved between calls to `encrypt` and `decrypt`. */
export type Crypted<T> = string & { __tag: T }

/** Stringify and encrypt a value, returning the resulting ciphertext.
  * @param key Encryption key.
  * @param val Value to encrypt.
  * @note Uses v8.serialize internally, so all of the same caveats
  *       about what types of value can and can't be serialised apply. */
export function encrypt<T> (key: BinaryLike, val: T): Crypted<T> {
  const vec = randomBytes(16)
  const cip = createCipheriv('aes-256-gcm', key, vec, { authTagLength: 16 })
  return Buffer.concat([
    vec,
    cip.update(serialize(val)),
    cip.final(),
    cip.getAuthTag()
  ]).toString('base64') as Crypted<T>
}

/** Decrypt and parse a ciphertext, returning the original value.
  * @param key Decryption key.
  * @param txt Ciphertext to decrypt. */
export function decrypt<T> (key: BinaryLike, txt: Crypted<T>): T
export function decrypt<T> (key: BinaryLike, txt: string): T

export function decrypt (key: BinaryLike, txt: string): string {
  const buf = Buffer.from(txt, 'base64')
  const dec = createDecipheriv('aes-256-gcm', key, buf.slice(0, 16)).setAuthTag(buf.slice(-16))
  return deserialize(Buffer.concat([
    dec.update(buf.slice(16, -16)),
    dec.final()
  ]))
}

/** Derive a key for use with `encrypt` and `decrypt`. */
export async function keygen (pass: BinaryLike, salt: BinaryLike): Promise<Buffer> {
  return await new Promise((resolve, reject) => {
    return pbkdf2(pass, salt, 100000, 32, 'sha512', (err, key) => {
      return err !== null
        ? reject(err)
        : resolve(key)
    })
  })
}

/** Synchronously derive a key for use with `encrypt` and `decrypt`. */
export function keygenSync (pass: BinaryLike, salt: BinaryLike): Buffer {
  return pbkdf2Sync(pass, salt, 100000, 32, 'sha512')
}
