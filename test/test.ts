
import { assert } from 'chai'
import {
  encrypt,
  decrypt,
  keygen,
  keygenSync,
  Crypted
} from '../src'

describe('Crypted', () => {
  describe('keygen', () => {
    it('generates keys', async () => {
      const key = await keygen('password', 'salt')
      expect(key).toBeInstanceOf(Buffer)
      expect(key.length).toEqual(32)
    })
    it('generates keys repeatably', async () => {
      const keyA = await keygen('password', 'salt')
      const keyB = await keygen('password', 'salt')
      expect(keyA).toStrictEqual(keyB)
    })
    it('incorporates password', async () => {
      const key = await keygen('password', 'salt')
      expect(key).not.toStrictEqual(await keygen('password1', 'salt'))
      expect(key).not.toStrictEqual(await keygen('password2', 'salt'))
      expect(key).not.toStrictEqual(await keygen('password3', 'salt'))
      expect(key).not.toStrictEqual(await keygen('password4', 'salt'))
    })
    it('incorporates salt', async () => {
      const key = await keygen('password', 'salt')
      expect(key).not.toStrictEqual(await keygen('password', 'salt1'))
      expect(key).not.toStrictEqual(await keygen('password', 'salt2'))
      expect(key).not.toStrictEqual(await keygen('password', 'salt3'))
      expect(key).not.toStrictEqual(await keygen('password', 'salt4'))
    })
  })
  describe('keygenSync', () => {
    it('generates keys', () => {
      const key = keygenSync('password', 'salt')
      expect(key).toBeInstanceOf(Buffer)
      expect(key.length).toEqual(32)
    })
    it('generates keys repeatably', () => {
      const keyA = keygenSync('password', 'salt')
      const keyB = keygenSync('password', 'salt')
      expect(keyA).toStrictEqual(keyB)
    })
    it('incorporates password', () => {
      const key = keygenSync('password', 'salt')
      expect(key).not.toStrictEqual(keygenSync('password1', 'salt'))
      expect(key).not.toStrictEqual(keygenSync('password2', 'salt'))
      expect(key).not.toStrictEqual(keygenSync('password3', 'salt'))
      expect(key).not.toStrictEqual(keygenSync('password4', 'salt'))
    })
    it('incorporates salt', () => {
      const key = keygenSync('password', 'salt')
      expect(key).not.toStrictEqual(keygenSync('password', 'salt1'))
      expect(key).not.toStrictEqual(keygenSync('password', 'salt2'))
      expect(key).not.toStrictEqual(keygenSync('password', 'salt3'))
      expect(key).not.toStrictEqual(keygenSync('password', 'salt4'))
    })
    it('produces the same output as keygen', async () => {
      const keyA = keygenSync('password', 'salt')
      const keyB = await keygen('password', 'salt')
      expect(keyA).toStrictEqual(keyB)
    })
  })
  describe('encrypt', () => {
    let key: Buffer
    beforeAll(() => {
      key = keygenSync('password', 'salt')
    })
    it('encrypts data', () => {
      const txt = encrypt(key, 'data')
      expect(typeof txt).toBe('string')
      expect(txt).toMatch(/^[a-zA-Z0-9/+]+={0,2}$/)
    })
    it('encrypts data using random ivs', () => {
      const txt = new Set<string>()
      for (let i = 0; i < 1000; i++) {
        txt.add(encrypt(key, 'data'))
      }
      expect(txt.size).toEqual(1000)
    })
    it('encrypts data of different types', () => {
      for (const data of [
        null,
        true, false,
        0, 12345,
        '', 'test',
        [], [1, 2, 3],
        {}, { a: 1, b: 2, c: 3 },
        new Map([['foo', 'foo'], ['bar', 'bar']]),
        new Set(['foo', 'bar']),
        new Date('2020'),
        /foo.+bar/
      ]) {
        expect(typeof encrypt(key, data)).toBe('string')
      }
    })
    it('rejects invalid key types', () => {
      const anycrypt = encrypt as any
      expect(() => anycrypt(null, 'data')).toThrow()
      expect(() => anycrypt(true, 'data')).toThrow()
      expect(() => anycrypt(1234, 'data')).toThrow()
      expect(() => anycrypt([], 'data')).toThrow()
      expect(() => anycrypt({}, 'data')).toThrow()
    })
    it('rejects invalid key lengths', () => {
      expect(() => encrypt('short', 'data')).toThrow()
      expect(() => encrypt('longlonglonglonglonglonglonglonglong', 'data')).toThrow()
      expect(() => encrypt(Buffer.alloc(10), 'data')).toThrow()
      expect(() => encrypt(Buffer.alloc(50), 'data')).toThrow()
    })
  })
  describe('decrypt', () => {
    let key: Buffer
    let txt: Crypted<string>
    beforeAll(() => {
      key = keygenSync('password', 'salt')
      txt = encrypt(key, 'data')
    })
    it('decrypts ciphertexts', () => {
      expect(decrypt(key, encrypt(key, 'data'))).toEqual('data')
    })
    it('decrypts ciphertexts for different types of data', () => {
      for (const data of [
        null,
        true, false,
        0, 12345,
        '', 'test',
        [], [1, 2, 3],
        {}, { a: 1, b: 2, c: 3 },
        new Map([['foo', 'foo'], ['bar', 'bar']]),
        new Set(['foo', 'bar']),
        new Date('2020'),
        /foo.+bar/
      ]) {
        assert.deepStrictEqual(decrypt(key, encrypt(key, data)), data)
      }
    })
    it('rejects incorrect keys', () => {
      expect(decrypt(key, txt)).toStrictEqual('data')
      expect(() => decrypt(keygenSync('test1', 'salt'), txt)).toThrow()
      expect(() => decrypt(keygenSync('test2', 'salt'), txt)).toThrow()
      expect(() => decrypt(keygenSync('test3', 'salt'), txt)).toThrow()
      expect(() => decrypt(keygenSync('test4', 'salt'), txt)).toThrow()
    })
    it('rejects invalid key types', () => {
      const anycrypt = decrypt as any
      expect(() => anycrypt(null, txt)).toThrow()
      expect(() => anycrypt(true, txt)).toThrow()
      expect(() => anycrypt(1234, txt)).toThrow()
      expect(() => anycrypt([], txt)).toThrow()
      expect(() => anycrypt({}, txt)).toThrow()
    })
    it('rejects invalid key lengths', () => {
      expect(() => decrypt('short', txt)).toThrow()
      expect(() => decrypt('longlonglonglonglonglonglonglonglong', txt)).toThrow()
      expect(() => decrypt(Buffer.alloc(10), txt)).toThrow()
      expect(() => decrypt(Buffer.alloc(50), txt)).toThrow()
    })
    it('rejects invalid ciphertexts', () => {
      const anycrypt = decrypt as any
      expect(() => anycrypt(key, null)).toThrow()
      expect(() => anycrypt(key, true)).toThrow()
      expect(() => anycrypt(key, 1234)).toThrow()
      expect(() => anycrypt(key, [])).toThrow()
      expect(() => anycrypt(key, {})).toThrow()
      expect(() => anycrypt(key, '')).toThrow()
      expect(() => anycrypt(key, 'notaciphertext')).toThrow()
      expect(() => anycrypt(key, '123' + encrypt(key, 'notaciphertextanymore'))).toThrow()
    })
  })
})
